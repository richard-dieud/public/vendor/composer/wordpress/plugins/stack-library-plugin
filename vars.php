<?php

define('RD_WPPLG_STACK_LIB_ROOTURL', plugin_dir_url(__FILE__));

define('RD_WPPLG_STACK_LIB_PREFIX',  'rd_wpplg_stacklib_');
define('RD_WPPLG_STACK_LIB_GLOBAL_KEY',  'rd_wpplg_stacklib');


// ----- PLUGIN PATHS

define('RD_WPPLG_STACK_LIB_ROOTDIR', plugin_dir_path(__FILE__));
define('RD_WPPLG_STACK_LIB_ASSETS_DIR', RD_WPPLG_STACK_LIB_ROOTDIR . 'assets/');
define('RD_WPPLG_STACK_LIB_TEMPLATE_DIR', RD_WPPLG_STACK_LIB_ROOTDIR . 'template/');
define('RD_WPPLG_STACK_LIB_CONFIG_DIR', RD_WPPLG_STACK_LIB_ROOTDIR . 'config/');

// ----- TAXONOMY
define("RD_WPPLG_STACK_LIB_TAX_STACK", "rd_wpplg_term_stack");

// SHORTCODE
define("RD_WPPLG_STACK_LIB_TAX_STACK_SHORTCODE", "stacklib_stack");
