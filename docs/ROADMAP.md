# BACKLOG

Add not filtered todos

## Priority : High (MUST)

## Priority : Medium (SHOULD)

- [ ] Add "excerpt" column to Stack
- [ ] Database - Enhance data normalization, avoid "Array (string)"
- [ ] Database - Data will be added in the form of serialization to guarantee their loyalty

## Priority : Low (COULD)

# VERSIONS

## BOILERPLATE_VERSION

### TODOS

Add todos abour this version

- [ ] Task 4 (P1)
- [ ] Task 5 (P3)
- [ ] Task 6 (P3)

### DONE

- [x] Task 1 (P1)
- [x] Task 2 (P1)
- [x] Task 3 (P2)

## Version 1 : PathFinder

### GOALS

- [ ] Créer une base de données préliminaire des stack différencié par type, comprenant des couleurs
- [ ] Afficher la liste des stack avec : nom, couleur, shortcode, type
- [ ] Afficher un template personnalisable de stack au niveau du FRONT
- [ ] Un fichier de configurayion pour définir le nom de shortcode ou encore les noms des classes

### TODOS

Add todos abour this version

- [ ] Shortcode - Define CSS class to let dev customize the template (P1)
- [ ] Shortcode - Secure arg values (user is devil) (P1)
- [ ] Shortcode - Name comes from configuration (P2)

### DONE

- [x] Plugin - Add a new Taxonomy for "Stack"
