<?php
/*
    Plugin Name: Stack Library by RD
    Plugin URI: https://store.rdieud.com/dev/wordpress/plugins/stack-library
    Description: Access a list of technical languages and frameworks to add to your posts or theme
*/

namespace Rd\Wp\Plugin\StackLibrary;

require_once 'vars.php';

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

use Rd\Vendor\Php\Autoloader\Autoloader;

$autoloader = new Autoloader();
$autoloader->register();

$autoloader->add_namespace('Rd\Wp\Plugin\StackLibrary\\', RD_WPPLG_STACK_LIB_ROOTDIR . "inc/");
$autoloader->add_namespace('Rd\Wp\Plugin\StackLibrary\\', RD_WPPLG_STACK_LIB_ROOTDIR . "inc/class/");


if (class_exists(Plugin::class)) {
    $instance = new Plugin();

    if (isset($instance)) {

        add_action('init', [$instance, 'onInit']);

        add_action('after_switch_theme', [$instance, 'rewriteFlush']);

        // Registers
        register_activation_hook(__FILE__, [$instance, 'onActivate']);
        register_deactivation_hook(__FILE__, [$instance, 'onDeactivate']);
    }
}
