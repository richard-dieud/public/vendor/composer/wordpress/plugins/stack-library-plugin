<?php

namespace Rd\Wp\Plugin\StackLibrary\Traits;

use Rd\Vendor\Wordpress\Factory\DbFactory;

use Rd\Wp\Plugin\StackLibrary\Model\Stack;

if (!trait_exists('Rd\Wp\Plugin\StackLibrary\Traits\AdminTrait')) {
    trait AdminTrait
    {
        public function adminInit()
        {
            $this->applyAdminAssets();

            if (function_exists('add_menu_page')) {
                add_menu_page(
                    __('Sample page'),
                    __('Stack Library'),
                    'manage_options',
                    'stack-library-by-rd',
                    [$this, 'renderAdminPage'],
                    'dashicons-schedule',
                    3
                );
            }
        }


        protected function registerAdminAssets()
        {
            // STYLES
            wp_register_style('rd_wp_plg_stacklib_assets_admin_main_styles', RD_WPPLG_STACK_LIB_ROOTURL . 'assets/styles/a.css');

            // SCRIPTS
            wp_register_script('rd_wp_plg_stacklib_assets_admin_main_scripts', RD_WPPLG_STACK_LIB_ROOTURL . 'assets/scripts/a.js');
        }

        protected function activateAdminAssets()
        {
            // STYLES
            wp_enqueue_style('rd_wp_plg_stacklib_assets_admin_main_styles');

            // SCRIPTS
            wp_enqueue_script('rd_wp_plg_stacklib_assets_admin_main_scripts');
        }

        public function applyAdminAssets()
        {
            $this->registerAdminAssets();
            $this->activateAdminAssets();
        }


        // ---------------- VIEW

        public function renderAdminPage()
        {
            $data = DbFactory::getList(Stack::class);

?>
            <h1>
                <?php esc_html_e('Stack Library by RD'); ?>
            </h1>

            <table class="wp-list-table widefat fixed striped table-view-list posts">
                <thead>
                    <tr>
                        <td class="sortable">Name</td>
                        <td class="sortable">Type</td>
                        <td>Color</td>
                        <td>Icon</td>
                        <td>Short Code</td>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($data as $row) : ?>
                        <tr>
                            <td><?= $row->getName() ?></td>
                            <td><?= $row->getType() ?></td>
                            <td>
                                <h6 class='rd-wp-plg-stack-lib view-stack view-stack-ovrd'>
                                    <span class='view-stack-color view-stack-color-ovrd' style='background-color: <?= $row->getColor() ?>'></span>
                                    <span class='view-stack-label view-stack-label-ovrd'><?= $row->getColor() ?></span>
                                </h6>
                            </td>
                            <td><?= $row->getIcon() ?></td>
                            <td>[rd_wp_plg_stack_lib_sc id="<?= $row->getSlug() ?>"]</td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
<?php
        }
    }
}
