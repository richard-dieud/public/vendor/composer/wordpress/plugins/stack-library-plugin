<?php

namespace Rd\Wp\Plugin\StackLibrary\Traits;

use Rd\Vendor\Wordpress\Factory\DbFactory;

use Rd\Wp\Plugin\StackLibrary\Model\Stack;

if (!trait_exists('Rd\Wp\Plugin\StackLibrary\Traits\ThemeTrait')) {
    trait ThemeTrait
    {
        public function themeInit()
        {
            $this->applyFrontAssets();
            if (function_exists('add_shortcode')) {
                add_shortcode(RD_WPPLG_STACK_LIB_TAX_STACK_SHORTCODE, [$this, 'renderShortCode']);
            }
        }

        protected function applyFrontAssets()
        {
            $this->registerFromAssets();
            $this->activateFrontAssets();
        }

        protected function registerFromAssets()
        {
            // STYLES
            wp_register_style('rd_wp_plg_stacklib_assets_theme_main_styles', RD_WPPLG_STACK_LIB_ROOTURL . 'assets/styles/f.css');

            // SCRIPTS
            wp_deregister_script('jquery');
            wp_register_script('jquery', 'https://code.jquery.com/jquery-3.5.1.slim.min.js', [], false, true);

            wp_register_script('rd_wp_plg_stacklib_assets_theme_main_scripts', RD_WPPLG_STACK_LIB_ROOTURL . 'assets/scripts/f.js', ['jquery']);
        }

        protected function activateFrontAssets()
        {
            // STYLES
            wp_enqueue_style('rd_wp_plg_stacklib_assets_theme_main_styles');

            // SCRIPTS
            wp_enqueue_script('rd_wp_plg_stacklib_assets_theme_main_scripts');
        }

        public function renderShortCode($args)
        {
            // @todo IMPORTANT add security !!!
            $res = DbFactory::getBy(Stack::class, $args['id'], "slug");

            $data = null;
            if (!empty($res) && is_array($res)) {
                $res = current($res);
                $data = $res->getProps(true);
            }

            $name = $data['name'];
            $color = $data['color'];

            return "
                <h6 class='rd-wp-plg-stack-lib view-stack view-stack-ovrd'>
                    <span class='view-stack-color view-stack-color-ovrd' style='background-color: $color'></span>
                    <span class='view-stack-label view-stack-label-ovrd'>$name</span>
                </h6>
            ";
        }
    }
}
