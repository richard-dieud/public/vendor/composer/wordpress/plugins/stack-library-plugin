<?php

/*
    NOTE: 
    Remember! Having a model class, even when we don't use it is part of our best practices.
    It helps our work and code to be interoperable in any case, any time.

    Properties are set to protected on purpose.
    It enables parent to get access to them through get_object_vars
*/

namespace Rd\Wp\Plugin\StackLibrary\Model;

use Rd\Vendor\Php\Model\Something;

if (!class_exists("Rd\Wp\Plugin\StackLibrary\Model\Stack")) {
    class Stack extends Something
    {
        const TABLE_NAME = "rd_wp_plugin_stacklib_stack";


        protected $name;

        protected $slug;

        protected $icon;

        protected $type;

        protected $content;

        protected $licence;

        protected $color;

        protected $tags;

        protected $github_trending_url;

        protected $website_url;

        protected $repository_url;

        /**
         * Get the value of name
         */
        public function getName()
        {
            return $this->name;
        }

        /**
         * Set the value of name
         *
         * @return  self
         */
        public function setName($name)
        {
            $this->name = $name;

            return $this;
        }

        /**
         * Get the value of slug
         */
        public function getSlug()
        {
            return $this->slug;
        }

        /**
         * Set the value of slug
         *
         * @return  self
         */
        public function setSlug($slug)
        {
            $this->slug = $slug;

            return $this;
        }

        /**
         * Get the value of icon
         */
        public function getIcon()
        {
            return $this->icon;
        }

        /**
         * Set the value of icon
         *
         * @return  self
         */
        public function setIcon($icon)
        {
            $this->icon = $icon;

            return $this;
        }

        /**
         * Get the value of type
         */
        public function getType()
        {
            return $this->type;
        }

        /**
         * Set the value of type
         *
         * @return  self
         */
        public function setType($type)
        {
            $this->type = $type;

            return $this;
        }

        /**
         * Get the value of content
         */
        public function getContent()
        {
            return $this->content;
        }

        /**
         * Set the value of content
         *
         * @return  self
         */
        public function setContent($content)
        {
            $this->content = $content;

            return $this;
        }

        /**
         * Get the value of licence
         */
        public function getLicence()
        {
            return $this->licence;
        }

        /**
         * Set the value of licence
         *
         * @return  self
         */
        public function setLicence($licence)
        {
            $this->licence = $licence;

            return $this;
        }

        /**
         * Get the value of color
         */
        public function getColor()
        {
            return $this->color;
        }

        /**
         * Set the value of color
         *
         * @return  self
         */
        public function setColor($color)
        {
            $this->color = $color;

            return $this;
        }

        /**
         * Get the value of tags
         */
        public function getTags()
        {
            return $this->tags;
        }

        /**
         * Set the value of tags
         *
         * @return  self
         */
        public function setTags($tags)
        {
            $this->tags = $tags;

            return $this;
        }

        /**
         * Get the value of github_trending_url
         */
        public function getGithubTrendingUrl()
        {
            return $this->github_trending_url;
        }

        /**
         * Set the value of github_trending_url
         *
         * @return  self
         */
        public function setGithubTrendingUrl($github_trending_url)
        {
            $this->github_trending_url = $github_trending_url;

            return $this;
        }

        /**
         * Get the value of website_url
         */
        public function getWebsiteUrl()
        {
            return $this->website_url;
        }

        /**
         * Set the value of website_url
         *
         * @return  self
         */
        public function setWebsiteUrl($website_url)
        {
            $this->website_url = $website_url;

            return $this;
        }

        /**
         * Get the value of repository_url
         */
        public function getRepositoryUrl()
        {
            return $this->repository_url;
        }

        /**
         * Set the value of repository_url
         *
         * @return  self
         */
        public function setRepositoryUrl($repository_url)
        {
            $this->repository_url = $repository_url;

            return $this;
        }

        /* ---------------------------- */

        public function getProps($skip_id = false)
        {
            return $this->_getProps($this, $skip_id);
        }

        public function getTableName()
        {
            return self::TABLE_NAME;
        }
    }
}
