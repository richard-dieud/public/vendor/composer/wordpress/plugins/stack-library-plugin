<?php

namespace Rd\Wp\Plugin\StackLibrary;

use Rd\Vendor\Wordpress\Factory\DbFactory;
use Rd\Wp\Plugin\StackLibrary\Model\Stack;

use Rd\Wp\Plugin\StackLibrary\Traits\AdminTrait;
use Rd\Wp\Plugin\StackLibrary\Traits\ThemeTrait;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

if (!class_exists("Rd\Wp\Plugin\StackLibrary\Plugin")) {
    class Plugin
    {
        use AdminTrait, ThemeTrait;

        private $wpdb;
        private $tableName;
        private $createTableQry;
        private $config;
        private $adminPageOptions;

        public function __construct()
        {
            global $wpdb;
            $this->wpdb = $wpdb;

            $this->checkDeps();

            $this->tableName = $this->wpdb->prefix . Stack::TABLE_NAME;

            $this->createTableQry = "
                CREATE TABLE `$this->tableName` (
                    `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
                    `name` CHAR(255) NOT NULL,
                    `slug` CHAR(255) NOT NULL,
                    `icon` CHAR(255),
                    `type` CHAR(255) NOT NULL,
                    `content` TEXT,
                    `licence` CHAR(255),
                    `color` CHAR(255),
                    `tags` CHAR(255),
                    `github_trending_url` CHAR(255),
                    `website_url` CHAR(255),
                    `repository_url` CHAR(255),
                    `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                    `updated_at` TIMESTAMP NULL
                ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci
            ";

            $configFile = RD_WPPLG_STACK_LIB_CONFIG_DIR . 'config.json';
            if (file_exists($configFile)) {
                $this->config = json_decode(file_get_contents($configFile));
            }
        }

        private function checkDeps()
        {
            if (
                // REQUIRED CLASSES
                !class_exists(DbFactory::class)
                || !class_exists(Stack::class)
                // PHP-VENDOR LIB
            ) {
                throw new \Error('Something went wrong : The plugin has a critical dependency issue');
            }
        }

        function getConfig()
        {
            return $this->config;
        }

        function addCustomObjects()
        {
            register_taxonomy(RD_WPPLG_STACK_LIB_TAX_STACK, 'post', [
                // Ref: https://developer.wordpress.org/reference/functions/get_taxonomy_labels/
                'labels'            => array(
                    'name'              =>  __('Stack'),
                    'singular_name'     =>  __('Stack'),
                    'plural_name'       =>  __('Stacks'),
                    'search_items'      =>  __('Search stacks'),
                    'all_items'         =>  __('All stacks'),
                    'edit_item'         =>  __('Edit a stack'),
                    'update_item'       =>  __('Update the stack'),
                    'add_new_item'      =>  __('Add a new stack'),
                    'new_item_name'     =>  __('New stack name'),
                    'menu_name'         =>  __('Stack'),
                ),
                "public"    => true,
                // Pour être accessible dans REST & l'editeur de bloc (car fonctionne avec REST)
                'show_in_rest'         => true,
                // Gère le système Parent/Enfant mais aussi afficher sous forme de liste avec Checkbox (c'est un hack d'un effet secondaire)
                'hierarchical'         => false,
                // Affiche la Taxo dans la colonne de la liste des articles
                'show_admin_column'     => true,
            ]);
        }

        /* ============ ============ DATA ============ ============ */

        private function createPreDataCollection()
        {
            $data = file_get_contents(RD_WPPLG_STACK_LIB_ROOTDIR . 'data/formatted.json');
            $data = json_decode($data);

            $stack_list = get_object_vars($data);
            $stack_list = array_values($stack_list);

            $results = DbFactory::hydrate(Stack::class, $stack_list);

            return $results;
        }

        /* ============ ============ HOOK HANDLERS ============ ============ */

        function onActivate()
        {
            if (!DbFactory::tableExists($this->tableName)) {
                // CreateTable
                DbFactory::createTable($this->tableName, trim($this->createTableQry));
            }

            // Insert Pre-Data
            $results = $this->createPreDataCollection();

            DbFactory::addCollection($results, Stack::class);

            // Ajouter les "terms" dans la base de données
            $data = DbFactory::getList(Stack::class);

            if (!taxonomy_exists(RD_WPPLG_STACK_LIB_TAX_STACK)) {
                $this->addCustomObjects();
            }

            if ($data && taxonomy_exists(RD_WPPLG_STACK_LIB_TAX_STACK)) {
                foreach ($data as $stack) {
                    $name = $stack->getName();
                    $slug = $stack->getSlug();

                    // dump($name, $slug);
                    // die();

                    if (!term_exists($name, RD_WPPLG_STACK_LIB_TAX_STACK)) {
                        wp_insert_term($name, RD_WPPLG_STACK_LIB_TAX_STACK, [
                            "slug" => $slug,
                        ]);
                    }
                }
            } else {
                // @todo Add rolling back function
                $this->onActiveRollback();
                throw new \Error("Couldn't add taxonomies, rolling back...");
            }
        }

        function  onActiveRollback()
        {
        }

        function onDeactivate()
        {
            // Ajouter les "terms" dans la base de données
            $data = DbFactory::getList(Stack::class);

            if ($data && taxonomy_exists(RD_WPPLG_STACK_LIB_TAX_STACK)) {
                foreach ($data as $stack) {
                    $name = $stack->getName();
                    if ($ids = term_exists($name, RD_WPPLG_STACK_LIB_TAX_STACK)) {
                        wp_delete_term($ids['term_id'], RD_WPPLG_STACK_LIB_TAX_STACK);
                    }
                }
            }

            unregister_taxonomy(RD_WPPLG_STACK_LIB_TAX_STACK);

            DbFactory::dropTable($this->tableName);
        }

        function onUninstall()
        {
            DbFactory::dropTable($this->tableName);
        }


        function onInit()
        {
            add_action('wp_enqueue_scripts', [$this, 'themeInit']);

            add_action('admin_menu', [$this, 'adminInit']);

            add_shortcode('rd_wp_plg_stack_lib_sc', [$this, 'renderShortCode']);

            // Add taxymony
            $this->addCustomObjects();
        }

        function rewriteFlush()
        {
            // Helps not manually refresh "Permalinks" structure by going to "Flushing"
            flush_rewrite_rules();
        }
    }
}
